import java.util.ArrayList;

public class ParkingArea {

    private final int capacity;

    public static final String PARK = "Car is parked";
    public static  final String NOT_PARK = "Car is not parked";
    public static final String UNPARK = "Car is unparked";
    public static  final String UNPARK_NOT_PRESENT = "Car to be unparked is not present";

    static ArrayList<Object> parkedCars = new ArrayList<>();

    public ParkingArea(int capacity) {
        this.capacity = capacity;
    }


    public String park(Object car) throws Exception {

        if (parkedCars.size() < capacity) {
            parkedCars.add(car);
            return PARK;
        }
        throw new Exception(NOT_PARK);
    }


    public String unpark(Object car) throws Exception {
        if (parkedCars.contains(car)) {
            parkedCars.remove(car);
            return UNPARK;
        }
        throw new Exception(UNPARK_NOT_PRESENT);
    }


}
