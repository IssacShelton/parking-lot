import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class ParkingAreaTest {

    @Test
    public void shouldReturnCarIsParkedMessageWhenCarIsParked() throws Exception {
        ParkingArea parkingArea = new ParkingArea(100);

        Object car = new Object();
        String actual = parkingArea.park(car);

        assertEquals(ParkingArea.PARK,actual);
    }

    @Test
    public void shouldThrowCarIsNotParkedExceptionMessageWhenCarIsNotParked() throws Exception {
        ParkingArea parkingArea = new ParkingArea(1);

        Object carOne = new Object();
        Object carTwo = new Object();
        parkingArea.park(carOne);

        Exception exception = assertThrows(Exception.class,()-> parkingArea.park(carTwo));

        assertEquals(exception.getMessage(),ParkingArea.NOT_PARK);
    }

    @Test
    public void shouldReturnCarIsUnparkedMessageWhenCarIsUnParked() throws Exception {
        ParkingArea parkingArea = new ParkingArea(2);

        Object car = new Object();
        parkingArea.park(car);

        String actual = parkingArea.unpark(car);

        assertEquals(ParkingArea.UNPARK,actual);

    }
    @Test
    public void shouldThrowCarToBeUnparkedIsNotPresentExceptionMessageWhenCarToBeUnParkedIsNotPresentInTheParkingArea() {
        ParkingArea parkingArea = new ParkingArea(1);

        Object car = new Object();

        Exception exception=assertThrows(Exception.class,()->parkingArea.unpark(car));
        assertEquals(exception.getMessage(),ParkingArea.UNPARK_NOT_PRESENT);

    }
}